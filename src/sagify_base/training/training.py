from __future__ import print_function
import json
from pathlib import Path
import joblib
import os
import pandas as pd
import torch
from sklearn.metrics import f1_score, roc_auc_score, accuracy_score, coverage_error, label_ranking_average_precision_score
from sagify.api.hyperparameter_tuning import log_metric
from sentence_transformers import SentenceTransformer, util
import numpy as np


def multi_label_metrics(y_true, y_pred):
    f1_micro_average = f1_score(y_true=y_true, y_pred=y_pred, average='micro')
    print(f'f1={f1_micro_average};')
    log_metric("f1", f1_micro_average)

    roc_auc = roc_auc_score(y_true, y_pred, average='micro')
    print(f'roc_auc={roc_auc};')
    log_metric("roc_auc", roc_auc)

    accuracy = accuracy_score(y_true, y_pred)
    print(f'accuracy={accuracy};')
    log_metric("accuracy", accuracy)

    coverage_err = coverage_error(y_true, y_pred)
    print(f'coverage_err={coverage_err};')
    log_metric("coverage_err", coverage_err)

    label_ranking_average_precision = label_ranking_average_precision_score(y_true, y_pred)
    print(f'label_ranking_average_precision={label_ranking_average_precision};')
    log_metric("label_ranking_average_precision", label_ranking_average_precision)


def combine_columns(row):
    return f"{str(row['title'])} {str(row['short_description'])}"


def extract_cpvs_from_full_cpv(local_cpvs, number_digits):
    divisions = []
    local_cpvs = [str(local_cpv) for local_cpv in local_cpvs]
    for local_cpv in local_cpvs:
        if len(local_cpv) < 7:
            continue
        if len(local_cpv) == 7:
            local_cpv = "0"+local_cpv
        for upper_class in range(2, number_digits+1):
            division = local_cpv[:upper_class]
            length_division = len(division)
            if length_division < number_digits:
                remaining_digits = number_digits - length_division
                division = division + "0"*remaining_digits
            if division not in divisions:
                divisions.append(division)
    return divisions


def get_top_k_closest_notices(embedder, embeddings_train, reference_dataframe, query, actual_cpvs, top_k=1):
    result = []
    query_embedding = embedder.encode(query, convert_to_tensor=True)
    cos_scores = util.cos_sim(query_embedding, embeddings_train)[0]
    top_results = torch.topk(cos_scores, k=top_k)
    list_of_closest_notices = []

    for score, idx in zip(top_results[0], top_results[1]):
        cpvs_closest_notice = reference_dataframe['cpvs'].iloc[int(idx)]
        list_of_closest_notices.extend(cpvs_closest_notice)
        result.append({"pred": cpvs_closest_notice, "score": score.tolist()})
    result.append({"actual_cpvs": actual_cpvs})
    return result


def compute_scores(results_similarities, list_of_cpvs):
    formatted_result_actual = []
    formatted_result_pred = []
    normalized_factor = 0
    for i in range(1, 21):
        normalized_factor += 1 / i

    for element in results_similarities:
        single_row_pred = [0.0] * len(list_of_cpvs)
        single_row_actual = [0.0] * len(list_of_cpvs)
        cpv_found_and_scores = {}
        position = 0
        for single_pred in element:
            if 'actual_cpvs' in single_pred.keys():
                actual_values = single_pred["actual_cpvs"]
                for actual_value in actual_values:
                    if actual_value in list_of_cpvs:
                        index_in_all_cpvs = list_of_cpvs.index(actual_value)
                        single_row_actual[index_in_all_cpvs] = 1.0
            else:
                position += 1
                pred_values = single_pred["pred"]
                score_pred = single_pred["score"]
                for pred_value in pred_values:
                    if not cpv_found_and_scores.get(pred_value):
                        cpv_found_and_scores[pred_value] = score_pred / position
                    else:
                        cpv_found_and_scores[pred_value] = cpv_found_and_scores[pred_value] + (score_pred / position)
        for key, value in cpv_found_and_scores.items():
            index_cpv = list_of_cpvs.index(key)
            normalized_score = value / normalized_factor
            single_row_pred[index_cpv] = normalized_score
        formatted_result_actual.append(single_row_actual)
        formatted_result_pred.append(single_row_pred)
    return formatted_result_actual, formatted_result_pred


def train(input_data_path: str, model_save_path: str, hyperparams_path: str = None):
    """
    The function to execute the training.

    :param input_data_path: [str], input directory path where all the training file(s) reside in
    :param model_save_path: [str], directory path to save your model(s)
    :param hyperparams_path: [optional[str], default=None], input path to hyperparams json file.
    Example:
        {
            "max_leaf_nodes": 10,
            "n_estimators": 200
        }
    """

    # Load data
    input_data_folder = Path(input_data_path)
    input_files = get_filenames(input_data_folder)
    if len(input_files) == 0:
        raise ValueError(f"No file found in {input_data_folder}")
    raw_data = [pd.read_json(file) for file in input_files]
    df = pd.concat(raw_data)
    print(f"Full dataset contains {len(df)} records.")

    df["title_description"] = df.apply(combine_columns, axis=1)
    df = df.drop_duplicates(subset=["title_description"], keep='last')

    size_full_dataset = len(df)
    index_train = int(size_full_dataset * 0.8)
    df_train = df.iloc[:index_train, :]
    print(f"Training set contains {len(df_train)} records.")

    df_test = df.iloc[index_train:, :]
    print(f"Test set contains {len(df_test)} records.")

    title_descriptions = df_train['title_description'].values.tolist()
    print('Loading embedder model')
    embedder_l6 = SentenceTransformer('all-MiniLM-L6-v2')
    print("Generating embeddings")
    embeddings_train = embedder_l6.encode(title_descriptions, convert_to_tensor=True)
    print("Generating list of CPV supported")
    df_train.loc[:, 'cpvs'] = df_train["cpvs"].apply(lambda x: extract_cpvs_from_full_cpv(x, 5))
    df_test.loc[:, 'cpvs'] = df_test["cpvs"].apply(lambda x: extract_cpvs_from_full_cpv(x, 5))

    labels = df_train['cpvs'].values.tolist()
    all_cpvs = []
    for line in labels:
        all_cpvs += line
    all_cpvs = list(set(all_cpvs))
    print(f"Supported CPVs: {len(all_cpvs)}")

    # Evaluate results
    results_similarities = []
    for index, row in df_test.iterrows():
        row_result = get_top_k_closest_notices(embedder_l6, embeddings_train, df_train, row["title_description"], row["cpvs"], 20)
        results_similarities.append(row_result)
    actual, prediction = compute_scores(results_similarities, all_cpvs)
    pred_np = np.array(prediction)
    binary_predictions = (pred_np >= 0.25).astype(int)
    multi_label_metrics(actual, binary_predictions)

    embeddings_train = embeddings_train.detach().cpu()

    # Save model
    joblib.dump(embeddings_train, os.path.join(model_save_path, "embeddings_reference.joblib"))
    joblib.dump(labels, os.path.join(model_save_path, "labels_reference.joblib"))
    joblib.dump(all_cpvs, os.path.join(model_save_path, "all_cpvs.joblib"))


def get_filenames(data_folder):
    return [os.path.join(data_folder, file) for file in os.listdir(data_folder)]
